<?php
class messageMod extends commonMod {
	//消息一览
	public function look() {
		$user_id  = $_POST['user_id'];
		$today = strtotime("today");
		$tomorrow = strtotime("today")+3600*24;
		if (empty ( $user_id ) ) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}

		//******************群消息列表*****************
		$group_member = $this->model->table("group_member")->where("user_id = '".$user_id."' ")->select();
		$group_ids = array();
		if ($group_member){//查找我的所有群，遍历群group_id
			foreach ($group_member as $key => $val){
				$group_ids[] = $val['group_id'];
			}
		}
		if ($group_ids){//如果查到我所有的群group_ids
			$group_ids = implode(",", $group_ids);//分割成字符串,查找属于我的群消息
			/* $sql = "SELECT * FROM app_group_info
            		where group_id in (".$group_ids.")
            		GROUP By group_id 
            		ORDER BY insert_time desc ";  */
			$sql = "SELECT S.* FROM 
					(SELECT * FROM app_group_info 
					WHERE group_id in (".$group_ids.") 
					ORDER BY insert_time DESC ) 
					S GROUP BY group_id";
			$group_info = $this->model->query($sql);
			//print_r($group_info);exit;
		}else {//如果没有我的群消息，则为空
			$group_info = array();
		}
		//var_dump($group_info);
		$sql_m = "SELECT * FROM app_new_info
				  where user_id = {$user_id} or rec_id = {$user_id}
				  ORDER BY insert_time desc ";
		$member_info = $this->model->query($sql_m);
		//$group_info  群消息
		//$member_info 个人消息
		if($group_info and $member_info){
		$tmp_sum = array_merge($group_info,$member_info);
		}elseif ($group_info){
			$tmp_sum = $group_info;
		}elseif ($member_info){
			$tmp_sum = $member_info;
		}
		$list_time = $this->sortByMultiCols ( $tmp_sum, array ( 'insert_time' => SORT_DESC ) );
		
		//print_r($list_time);exit;
		if($list_time){//如果消息，则遍历我的最新群消息内容
			foreach ($list_time as $key => $value) {
				//按时间排序，查找我的最新消息
				if($value['group_id']){
					//print_r($val);exit;
					$group = $this->model->table('group_info')->where("group_id = '".$value['group_id']."' and if_read = 2")->order("insert_time desc")->find();
				
					$tmp[$key]['group_id']       = $group['group_id'];
					//查找群名
					$member_name = $this->model->table("group")->where("group_id = '".$value['group_id']."' ")->find();
					$tmp[$key]['group_name']     = $member_name['group_name'];
					if($group['info_img']){//如果消息是图片则最新消息内容显示为   [图片消息]
						$tmp[$key]['ginfo_img']      = "[イメージメッセージ]";
					}
					$tmp[$key]['ginfo_content']  = $group['info_content'];
					if($group['insert_time']>=$today&&$group['insert_time']<=$tomorrow){//如果消息为今天发的则时间格式为H:i 
						$tmp[$key]['insert_time']    = date("Y-m-d H:i",$group['insert_time']);
					}else{//如果消息为今天发的则时间格式为m-d
						$tmp[$key]['insert_time']    = date("Y-m-d H:i",$group['insert_time']);
					}
					//群成员数量
					$group_sum = $this->model->table('group_member')->where("group_id = '".$value['group_id']."' ")->count();
					$tmp[$key]['ginfo_sum']      = $group_sum;
					$tmp[$key]['type']      = "1";
					unset($group);
					unset($member_name);
					unset($group_sum);
				}else{
					
					if($user_id == $value['user_id']){
						$tmp[$key]['user_id'] 	= $value['rec_id'];//最后发消息的人id
						$user_name = $this->model->table("member")->where("user_id = '".$value['rec_id']."' ")->find();
						//$tmp[$key]['rec_id'] 	= $value['user_id'];
					}else{
						$tmp[$key]['user_id'] 	= $value['user_id'];//最后发消息的人id
						$user_name = $this->model->table("member")->where("user_id = '".$value['user_id']."' ")->find();
						//$tmp[$key]['rec_id'] 	= $value['rec_id'];
					}
					//查找最新消息的昵称
					
					
					
					if($user_name){//如果存在此发消息的用户
						$tmp[$key]['user_name']  = $user_name['user_nick'];
					}
					if($value['info_img']){//如果消息是图片则最新消息内容显示为   [图片消息]
						$tmp[$key]['minfo_img']      = "[イメージメッセージ]";
					}
					$tmp[$key]['minfo_content']  = $value['info_content'];
					if($value['insert_time']>=$today&&$value['insert_time']<=$tomorrow){//如果消息为今天发的则时间格式为H:i
						$tmp[$key]['insert_time_m']    = date("Y-m-d H:i",$value['insert_time']);
					}else{//如果消息为今天发的则时间格式为m-d
						$tmp[$key]['insert_time_m']    = date("Y-m-d H:i",$value['insert_time']);
					}
					$tmp[$key]['type']      = "2";
					unset($user_name);
				}
				
			}
		}
		//******************个人消息列表*****************
		/* $sql_m = "SELECT * FROM app_new_info
            	      where user_id = {$user_id} or rec_id = {$user_id} 
            	      ORDER BY insert_time desc ";
		$member_info = $this->model->query($sql_m); 
		if($member_info){//如果有我的消息，遍历我的最新的一条消息内容
			foreach ($member_info as $k => $v) {
				if($user_id == $v['user_id']){
					$tmp['mlist'][$k]['user_id'] 	= $v['rec_id'];
				}else{
					$tmp['mlist'][$k]['user_id'] 	= $v['user_id'];
				}
				//查找最新消息的昵称
				$user_name = $this->model->table("member")->where("user_id = '".$v['user_id']."' ")->find();
				if($user_name){//如果存在此发消息的用户
					$tmp['mlist'][$k]['user_name']  = $user_name['user_nick'];
				}
				if($v['info_img']){//如果消息是图片则最新消息内容显示为   [图片消息]
					$tmp['mlist'][$k]['minfo_img']      = "[イメージメッセージ]";
				}
				$tmp['mlist'][$k]['minfo_content']  = $v['info_content'];
				if($v['insert_time']>=$today&&$v['insert_time']<=$tomorrow){//如果消息为今天发的则时间格式为H:i 
					$tmp['mlist'][$k]['insert_time_m']    = date("H:i",$v['insert_time']);
				}else{//如果消息为今天发的则时间格式为m-d
					$tmp['mlist'][$k]['insert_time_m']    = date("m-d",$v['insert_time']);
				}
				unset($user_name);
			}
		}*/
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	public function send_admin_msg() {
		$user_id='999999999';
		$group_id='0';
		$rec_id = $_POST ['rec_id'];
		$info_content = $_POST ['info_content'];

		$data = array (
				"user_id" => '999999999',
				"rec_id" => $rec_id,
				"info_content" => $info_content,
				"insert_time" => time () 
		);
		$info_id = $this->model->table ( "group_info" )->data ( $data )->insert ();
		// ------------添加代码----------------
		$new_info = $this->model->table ( "new_info" )->where ( " (user_id = $user_id and rec_id = $rec_id) or (user_id = $rec_id and rec_id = $user_id) " )->find ();
		
		$data_new = array (
				"user_id" => $user_id,
				"rec_id" => $rec_id,
				"info_content" => $info_content,
				"insert_time" => time () 
		);
		if ($new_info) {
			$this->model->table ( "new_info" )->where ( " (user_id = $user_id and rec_id = $rec_id) or (user_id = $rec_id and rec_id = $user_id) " )->data ( $data_new )->update ();
		} elseif (! $new_info) {
			$this->model->table ( "new_info" )->data ( $data_new )->insert ();
		}

		$if_group = "2";
		$user = module ( "push" )->user_info ( $rec_id );
		if ($user ['device_id']) {
			$push_data = array (
					"info_type" => $if_group,
					"info_content" => $info_content,
					"info_img" => $info_img,
					"send_time" => date ( "Y-m-d H:i", time () ),
					"user_id" => $user_id,
					"rec_id" => $rec_id 
			);
			
			module ( "push" )->push ( "新しいメッセージ！", $push_data, $user ['device_id'], $user ['device'] );
		}
	}
}