<?php
define ( 'appkeys', '0547c24cc9c56f820cc9c903' ); // appkey值 极光portal上面提供
define ( 'masterSecret', '4ffc50d24f8c0184db3fc36e' ); // API MasterSecert值 极光portal上面提供
define ( 'platform', 'all' ); // 推送平台
class pushMod extends commonMod {
	private $_appkeys = appkeys;
	private $_masterSecret = masterSecret;
	private function request_post($url = "", $param = "", $header = "") {
		if (empty ( $url ) || empty ( $param )) {
			return false;
		}
		$postUrl = $url;
		$curlPost = $param;
		$ch = curl_init (); // 初始化curl
		curl_setopt ( $ch, CURLOPT_URL, $postUrl ); // 抓取指定网页
		curl_setopt ( $ch, CURLOPT_HEADER, 0 ); // 设置header
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 ); // 要求结果为字符串且输出到屏幕上
		curl_setopt ( $ch, CURLOPT_POST, 1 ); // post提交方式
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $curlPost );
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, $header );
		// 增加 HTTP Header（头）里的字段
		curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, FALSE );
		// 终止从服务端进行验证
		curl_setopt ( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
		$data = curl_exec ( $ch ); // 运行curl
		                           // var_dump($data);
		curl_close ( $ch );
		return $data;
	}
	public function send($content_json) {
		$url = 'https://api.jpush.cn/v3/push';
		$base64 = base64_encode ( "$this->_appkeys:$this->_masterSecret" );
		$header = array (
				"Authorization:Basic $base64",
				"Content-Type:application/json" 
		);
		$param = $content_json;
		$res = $this->request_post ( $url, $param, $header );
		$res_arr = json_decode ( $res, true );
		// print_r($res_arr);
	}
	public function push($title, $content, $device_id, $type, $uid = null) {
		$user_info = $this->model->table ( "member" )->where ( " device_id = '" . $device_id . "' " )->find ();
		if ($user_info ['push_flg'] == '1') {
			if ($type == '1') {
				$user = $this->user_info ( $content ['user_id'] );
				$data = array (
						"title" => $title,
						"type" => $content ['info_type'],
						"info_content" => $content ['info_content'],
						"info_img" => $content ['info_img'],
						"send_time" => $content ['send_time'],
						"user_id" => $content ['user_id'],
						"group_id" => $content ['group_id'],
						"rec_id" => $content ['rec_id'],
						"user_name" => $user ['user_nick'] 
				);
				$content_array = array (
						"platform" => platform,
						"audience" => array (
								"registration_id" => array (
										$device_id 
								) 
						),
						"message" => array (
								"msg_content" => $data,
								"title" => $title,
								"type" => $content ['info_type'],
								"info_content" => $content ['info_content'],
								"info_img" => $content ['info_img'],
								"send_time" => $content ['send_time'],
								"user_id" => $content ['user_id'],
								"group_id" => $content ['group_id'],
								"rec_id" => $content ['rec_id'],
								"user_name" => $user ['user_nick'] 
						) 
				);
				$content_json = $this->JSON ( $content_array );
				$this->send ( $content_json );
			} elseif ($type == '2') {
				$a = "true";
				$content_array = array (
						"platform" => platform,
						"audience" => array (
								"registration_id" => array (
										$device_id 
								) 
						),
						"notification" => array (
								"ios" => array (
										"alert" => $title,
										"sound" => "default",
										"badge" => 1,
										"extras" => array (
												"type" => $content ['info_type'],
												"info_content" => $content ['info_content'],
												"info_img" => $content ['info_img'],
												"send_time" => $content ['send_time'],
												"user_id" => $content ['user_id'],
												"group_id" => $content ['group_id'],
												"rec_id" => $content ['rec_id'] 
										) 
								) 
						),
						"options" => array (
								"apns_production" => $a 
						) 
				);
				if ($content ['type'] == "new_msg_incoming") {
					$content_array ['message'] = array (
							"msg_content" => $content,
							"title" => $title 
					);
					$content_array ["notification"] ["ios"] ["extras"] ["user_id"] = $uid;
				}
				$content_json = $this->JSON ( $content_array );
				
				$this->send ( $content_json );
			}
		}
	}
	public function test_push() {
		$title = $_POST ['title'];
		$content = $_POST ['content'];
		$device_id = $_POST ['device_id'];
		$content_array = array (
				"platform" => platform,
				"audience" => array (
						"registration_id" => array (
								$device_id 
						) 
				),
				"message" => array (
						"msg_content" => $content,
						"title" => $title 
				) 
		);
		$content_json = $this->JSON ( $content_array );
		$this->send ( $content_json );
	}
	public function admin_push() {
		$title = $_POST ['title'];
		$content = $_POST ['content'];
		$device_id = $_POST ['device_id'];
		$content_array = array (
				"platform" => platform,
				"audience" => array (
						"registration_id" => array (
								$device_id 
						) 
				),
				"message" => array (
						"msg_content" => $content,
						"title" => $title 
				) 
		);
		$content_json = $this->JSON ( $content_array );
		$this->send ( $content_json );
	}
	public function test_push1() {
		$title = $_POST ['title'];
		$content = $_POST ['content'];
		$device_id = "001f810d030";
		$content_array = array (
				"platform" => platform,
				"audience" => array (
						"registration_id" => array (
								$device_id 
						) 
				),
				"notification" => array (
						"ios" => array (
								"alert" => $content,
								"sound" => "default",
								"badge" => 1,
								"extras" => array (
										"news_id" => 1,
										"my_key" => "a value" 
								) 
						) 
				),
				"message" => array (
						"msg_content" => $content,
						"title" => $title 
				),
				"options" => array (
						"apns_production" => false 
				) 
		);
		$content_json = json_encode ( $content_array, true );
		// echo ($content_json);
		$this->send ( $content_json );
	}
	public function user_info($uid) {
		$info = $this->model->table ( "member" )->where ( "user_id = '" . $uid . "' " )->find ();
		return $info;
	}
}