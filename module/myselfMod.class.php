<?php
class myselfMod extends commonMod {
	// 我的资料
	public function mydata() {
		$user_id = $_POST ['user_id'];
		if (empty ( $user_id )) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$user_info = $this->model->table ( "member" )->where ( "user_id = '" . $user_id . "' " )->find ();
		if (! $user_info) { // 如果查不到用户
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "該当ユーザーがありません";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		// 按照我的子类划分12个模块，来计算资料填写百分比
		if (! empty ( $user_info ['user_sex'] )) {
			$a = "1";
		} else {
			$a = "0";
		}
		// 2
		if (! empty ( $user_info ['address'] )) {
			$b = "1";
		} else {
			$b = "0";
		}
		// 3
		if (! empty ( $user_info ['industry'] )) {
			$c = "1";
		} else {
			$c = "0";
		}
		// 4
		if (! empty ( $user_info ['size_company'] )) {
			$d = "1";
		} else {
			$d = "0";
		}
		// 5
		if (! empty ( $user_info ['job'] )) {
			$e = "1";
		} else {
			$e = "0";
		}
		// 6
		if (! empty ( $user_info ['reg_money'] )) {
			$f = "1";
		} else {
			$f = "0";
		}
		// 7
		if (! empty ( $user_info ['calendar'] )) {
			$g = "1";
		} else {
			$g = "0";
		}
		// 8
		if (! empty ( $user_info ['software'] )) {
			$h = "1";
		} else {
			$h = "0";
		}
		// 9
		if (! empty ( $user_info ['hardware'] )) {
			$i = "1";
		} else {
			$i = "0";
		}
		// 10
		if (! empty ( $user_info ['qualified'] )) {
			$j = "1";
		} else {
			$j = "0";
		}
		// 11
		if (! empty ( $user_info ['allyear'] )) {
			$k = "1";
		} else {
			$k = "0";
		}
		// 12
		if (! empty ( $user_info ['introduction'] )) {
			$n = "1";
		} else {
			$n = "0";
		}
		$o = ($a + $b + $c + $d + $e + $f + $g + $h + $i + $j + $k + $l + $m + $n) / 12 * 100;
		$scale = floor ( $o ); // 取整
		$tmp ['user_nick'] = $user_info ['user_nick'];
		$tmp ['user_email'] = $user_info ['user_email'];
		$tmp ['user_sex'] = $user_info ['user_sex']; // 性别
		
		$address = $this->model->table ( 'area' )->where ( "area_id = '" . $user_info ['address'] . "' " )->find ();
		$tmp ['address'] = $address ['area_name']; // 地址
		$tmp ['area_id'] = $user_info ['address']; // 地址
		
		$industry = $this->model->table ( 'industry' )->where ( "industry_id = '" . $user_info ['industry'] . "' " )->find ();
		$tmp ['industry'] = $industry ['industry_name']; // 职业种类
		$tmp ['industry_id'] = $user_info ['industry']; // 职业种类
		
		$size_company = $this->model->table ( 'size_company' )->where ( "size_company_id = '" . $user_info ['size_company'] . "' " )->find ();
		$tmp ['size_company'] = $size_company ['size_company_name']; // 公司规模
		$tmp ['size_company_id'] = $user_info ['size_company']; // 公司规模
		
		$jobs = $this->model->table ( 'jobs' )->where ( "job_id = '" . $user_info ['job'] . "' " )->find ();
		$tmp ['job'] = $jobs ['name']; // 职务
		$tmp ['job_id'] = $user_info ['job']; // 职务
		
		$reg_money = $this->model->table ( 'reg_money' )->where ( "reg_money_id = '" . $user_info ['reg_money'] . "' " )->find ();
		$tmp ['reg_money'] = $reg_money ['reg_money_name']; // 注册资金
		$tmp ['reg_money_id'] = $user_info ['reg_money']; // 注册资金
		
		$calendar = $this->model->table ( 'use' )->where ( "use_id = '" . $user_info ['calendar'] . "' " )->find ();
		$tmp ['calendar'] = $calendar ['time_name']; // 使用时间
		$tmp ['use_id'] = $user_info ['calendar']; // 使用时间

		//$software = $this->model->table ( 'software' )->where ( "software_id = '" . $user_info ['software'] . "' " )->find ();
		//$tmp ['software'] = $software ['software_name']; // 软件
		$arr_software=explode(",",$user_info ['software']);
		for($i=0;$i<count($arr_software);$i++){
			$software = $this->model->table ( 'software' )->where ( "software_id = '" . $arr_software [$i] . "' " )->find ();
			$tmp ['software'][$i] = $software ['software_name']; // 软件
		}
		$tmp ['software']=implode(",",$tmp ['software']);
		$tmp ['software_id']=$user_info ['software'];
		
		//$hardware = $this->model->table ( 'hardware' )->where ( "hardware_id = '" . $user_info ['hardware'] . "' " )->find ();
		//$tmp ['hardware'] = $hardware ['hardware_name']; // 硬件
		$arr_hardware=explode(",",$user_info ['hardware']);
		for($i=0;$i<count($arr_hardware);$i++){
			$hardware = $this->model->table ( 'hardware' )->where ( "hardware_id = '" . $arr_hardware [$i] . "' " )->find ();
			$tmp ['hardware'][$i] = $hardware ['hardware_name']; // 硬件
		}
		$tmp ['hardware']=implode(",",$tmp ['hardware']);
		$tmp ['hardware_id']=$user_info ['hardware'];
		//$qualified = $this->model->table ( 'qualified' )->where ( "qualified_id = '" . $user_info ['qualified'] . "' " )->find ();
		//$tmp ['qualified'] = $qualified ['qualified_name']; // 拥有资格
		$arr_qualified=explode(",",$user_info ['qualified']);
		for($i=0;$i<count($arr_qualified);$i++){
			$qualified = $this->model->table ( 'qualified' )->where ( "qualified_id = '" . $arr_qualified [$i] . "' " )->find ();
			$tmp ['qualified'][$i] = $qualified ['qualified_name']; // 拥有资格
		}
		$tmp ['qualified']=implode(",",$tmp ['qualified']);
		$tmp ['qualified_id']=$user_info ['qualified'];
		$allyear = $this->model->table ( 'allyear' )->where ( "allyear_id = '" . $user_info ['allyear'] . "' " )->find ();
		$tmp ['allyear'] = $allyear ['allyear_name']; // 全年预算
		
		$number = $this->model->table ( 'fans' )->where ( "uid_a = '" . $user_id . "' " )->count ();
		$tmp ['number'] = $number; // 我的好友
		
		$price1 = 0;//鸟点赞
		$price2 = 0;//知恵袋点赞
		$price3 = 0;// 知恵袋评论点赞
		$brid = $this->model->table("brid")->where("user_id = $user_id " )->select();
		//var_dump($brid);
		$know  = $this->model->table("know")->where("user_id = $user_id " )->select();
		$know_comments = $this->model->table("know_comments")->where("user_id = $user_id " )->select();
		//dump($know);
		 if ($brid){
			$brid_ids = $this->i_array_column($brid, "id");
			$brid_ids = array_filter($brid_ids);
			$brid_ids = implode(",", $brid_ids);
			$price1 = $this->model->table ( 'good' )->where ( "info_id in (".$brid_ids.") and type = 2" )->count ();
		}
		if ($know){
			$know_ids = $this->i_array_column($know, "id");
			$know_ids = array_filter($know_ids);
			$know_ids = implode(",", $know_ids);
			//var_dump($know_ids);exit();
			$price2 = $this->model->table ( 'good' )->where ( "info_id in (".$know_ids.") and type = 1 and c_id=0 " )->count ();
			//var_dump($price2);
		}
		
	if ($know_comments){
			$know_comments_ids = $this->i_array_column($know_comments, "id");
			$know_comments_ids = array_filter($know_comments_ids);
			$know_comments_ids = implode(",", $know_comments_ids);
			//var_dump($know_comments_ids);exit();
			$price3 = $this->model->table ( 'good' )->where ( " c_id in (".$know_comments_ids.") and type = 1" )->count ();
			//var_dump($price3);exit();
	} 
		
		$price = $price1 + $price2 +$price3;
		//$price = $price1 + $price3;
		$tmp ['price'] = $price; // 我的点赞数
		if ($user_info ['insert_time']) {
			$tmp ['time'] = date ( "Y-m-d", $user_info ['insert_time'] );
		}
		$tmp ['introduction'] = $user_info ['introduction'];
		$tmp ['scale'] = $scale;
		$tmp ['mail_flg'] = $user_info ['mail_flg'];
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	// 我发布的文章
	public function myinfolist() {
		$p_num = $_POST ['p_num'];
		$p_size = $_POST ['p_size'];
		$user_id = $_POST ['user_id'];
		if (empty ( $p_size ) || empty ( $user_id )) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		if (empty ( $p_num )) { // 页码默认为第一页
			$p_num = 1;
		}
		$str = ($p_num - 1) * $p_size;
		
		// 我评论过的文章
		$sql_c = "select * from app_know_comments 
				  where user_id = $user_id 
				  group by info_id ";
		$list_c = $this->model->query ( $sql_c );
		if ($list_c) {
			$kc = array ();
			foreach ( $list_c as $key => $val ) {
				$kc [] = $val ['info_id'];
			}
		}
		if ($kc) {
			$kc_id = implode ( ',', $kc ); // 我评论过的文章id
			                           // 我发布过的文章或评论过的文章
			$list_k = $this->model->table ( "know" )->where ( " user_id = '" . $user_id . "' or id in ({$kc_id}) " )->limit ( "$str" . "," . "$p_size" )->order ( "insert_time desc" )->select ();
		} else {
			// 我发布过的文章
			$list_k = $this->model->table ( "know" )->where ( " user_id = '" . $user_id . "'  " )->limit ( "$str" . "," . "$p_size" )->order ( "insert_time desc" )->select ();
		}
		if ($list_k) { // 如果我发布过文章，则统计回复数
			foreach ( $list_k as $k => $vo ) {
				$count = $this->model->table ( "know_comments" )->where ( "info_id = '" . $vo ['id'] . "' " )->count ();
				$list_k [$k] ['c_n'] = $count;
				unset ( $count );
			}
		}
		$kmp = array ();
		if ($list_k) { // 如果我发布过文章，则进行信息遍历
			foreach ( $list_k as $key => $val ) {
				$kmp [$key] ['info_id'] = $val ['id'];
				$kmp [$key] ['title'] = $val ['title'];
				$kmp [$key] ['comment_sum'] = $val ['c_n'];
				// $kmp [$key] ['urgent'] = $val ['urgent'];
				// $kmp [$key] ['time'] = date ( "Y-m-d H:i", $val ['insert_time'] );
				$kmp [$key] ['time'] = $val ['insert_time'];
				$kmp [$key] ['type'] = "1";
			}
		}
		
		$sql_d = "select * from app_brid_comments
			      where user_id = $user_id
				  group by bird_id ";
		$list_d = $this->model->query ( $sql_d );
		if ($list_d) {
			$bc = array ();
			foreach ( $list_d as $key => $val ) {
				$bc [] = $val ['bird_id'];
			}
		}
		/*
		 * if($bc){
		 * $bc_id = implode(',',$bc); //我评论过的微博id
		 * //我发不过的微博或评论过的微博
		 * $list_b = $this->model->table ( "brid" )->where ( " user_id = '" . $user_id . "' or id in ({$bc_id}) " )->limit ( "$str" . "," . "$p_size" )->order ( "insert_time desc" )->select ();
		 *
		 * }else{
		 * //我发布过的微博
		 * $list_k = $this->model->table ( "brid" )->where ( " user_id = '" . $user_id . "' " )->limit ( "$str" . "," . "$p_size" )->order ( "insert_time desc" )->select ();
		 *
		 * }
		 */
		if ($bc) {
			$bc_id = implode ( ',', $bc ); // 我评论过的微博id
			if ($_POST ['user_id']) {
				$sql = "
				SELECT A.*,B.user_nick
				FROM {$this->model->pre}brid A
				LEFT JOIN {$this->model->pre}member B ON A.user_id = B.user_id
				WHERE  A.user_id  ='" . $_POST ['user_id'] . "' or A.id in ({$bc_id}) 
				ORDER By A.insert_time DESC
				LIMIT " . $str . "," . $p_size . "
					";
				$list_b = $this->model->query ( $sql );
			}
		} else {
			if ($_POST ['user_id']) {
				$sql = "
				SELECT A.*,B.user_nick
				FROM {$this->model->pre}brid A
				LEFT JOIN {$this->model->pre}member B ON A.user_id = B.user_id
				WHERE  A.user_id  ='" . $_POST ['user_id'] . "'
				ORDER By A.insert_time DESC
				LIMIT " . $str . "," . $p_size . "
				";
				$list_b = $this->model->query ( $sql );
			}
		}
		if ($list_b) { // 如果我发布过微博，则统计回复数
			foreach ( $list_b as $k => $vo ) {
				$count = $this->model->table ( "brid_comments" )->where ( "bird_id = '" . $vo ['id'] . "' " )->count ();
				$list_b [$k] ['c_n'] = $count;
				unset ( $count );
			}
		}
		
		$bmp = array ();
		if ($list_b) {
			foreach ( $list_b as $key => $val ) {
				$bmp [$key] ['info_id'] = $val ['id'];
				// $tmp [$key] ['content'] = $this->special_filter($val ['content']);
				$bmp [$key] ['title'] = $val ['content'];
				// $bmp [$key] ['user_name'] = $val ['user_nick'];
				$bmp [$key] ['comment_sum'] = $val ['c_n'];
				// $bmp [$key] ['time'] = date ( "Y-m-d H:i", $val ['insert_time'] );
				$bmp [$key] ['time'] = $val ['insert_time'];
				
				// $bmp [$key] ['user_id'] = $val ['user_id'];
				$bmp [$key] ['type'] = "2";
			}
		}
		/*
		 * $tmp['k'] = $kmp;
		 * $tmp['b'] = $bmp;
		 */
		
		$tmp_sum = array_merge ( $kmp, $bmp );
		
		$list_time = $this->sortByMultiCols ( $tmp_sum, array (
				'time' => SORT_DESC 
		) );
		
		// print_r($list);exit;
		$tmp = array ();
		if ($list_time) {
			foreach ( $list_time as $key_time => $val_time ) {
				$tmp [$key_time] ['info_id'] = $val_time ['info_id'];
				$tmp [$key_time] ['title'] = $val_time ['title'];
				$tmp [$key_time] ['comment_sum'] = $val_time ['comment_sum'];
				$tmp [$key_time] ['time'] = date ( "Y-m-d H:i", $val_time ['time'] );
				$tmp [$key_time] ['type'] = $val_time ['type'];
			}
		}
		
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	public function validate_email($email, $strict = true) {
			$dot_string = $strict ?
					'(?:[A-Za-z0-9!#$%&*+=?^_`{|}~\'\\/-]|(?<!\\.|\\A)\\.(?!\\.|@))' :
					'(?:[A-Za-z0-9!#$%&*+=?^_`{|}~\'\\/.-])'
			;
			$quoted_string = '(?:\\\\\\\\|\\\\"|\\\\?[A-Za-z0-9!#$%&*+=?^_`{|}~()<>[\\]:;@,. \'\\/-])';
			$ipv4_part = '(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])';
			$ipv6_part = '(?:[A-fa-f0-9]{1,4})';
			$fqdn_part = '(?:[A-Za-z](?:[A-Za-z0-9-]{0,61}?[A-Za-z0-9])?)';
			$ipv4 = "(?:(?:{$ipv4_part}\\.){3}{$ipv4_part})";
			$ipv6 = '(?:' .
					"(?:(?:{$ipv6_part}:){7}(?:{$ipv6_part}|:))" . '|' .
					"(?:(?:{$ipv6_part}:){6}(?::{$ipv6_part}|:{$ipv4}|:))" . '|' .
					"(?:(?:{$ipv6_part}:){5}(?:(?::{$ipv6_part}){1,2}|:{$ipv4}|:))" . '|' .
					"(?:(?:{$ipv6_part}:){4}(?:(?::{$ipv6_part}){1,3}|(?::{$ipv6_part})?:{$ipv4}|:))" . '|' .
					"(?:(?:{$ipv6_part}:){3}(?:(?::{$ipv6_part}){1,4}|(?::{$ipv6_part}){0,2}:{$ipv4}|:))" . '|' .
					"(?:(?:{$ipv6_part}:){2}(?:(?::{$ipv6_part}){1,5}|(?::{$ipv6_part}){0,3}:{$ipv4}|:))" . '|' .
					"(?:(?:{$ipv6_part}:){1}(?:(?::{$ipv6_part}){1,6}|(?::{$ipv6_part}){0,4}:{$ipv4}|:))" . '|' .
					"(?::(?:(?::{$ipv6_part}){1,7}|(?::{$ipv6_part}){0,5}:{$ipv4}|:))" . 
			')';
			$fqdn = "(?:(?:{$fqdn_part}\\.)+?{$fqdn_part})";
			$local = "({$dot_string}++|(\"){$quoted_string}++\")";
			$domain = "({$fqdn}|\\[{$ipv4}]|\\[{$ipv6}]|\\[{$fqdn}])";
			$pattern = "/\\A{$local}@{$domain}\\z/";
			return preg_match($pattern, $email, $matches) &&
					(
							!empty($matches[2]) && !isset($matches[1][66]) && !isset($matches[0][256]) ||
							!isset($matches[1][64]) && !isset($matches[0][254])
					)
			;
	}
	// 我的资料修改
	public function mydata_edit() {
		$user_id = $_POST ['user_id'];
		if (empty ( $user_id )) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$user_info = $this->model->table ( "member" )->where ( "user_id = '" . $user_id . "' " )->find ();
		if ($user_info) { // 如果存在用户，则进行接收值,is_numeric 判断接收值为数字
			$arr = array ();
			//$pattern = "/^([0-9A-Za-z\\-_\\.]+)@([0-9A-Za-z\\-_\\.]+\\.[a-z]{2,3}(\\.[a-z]{2})?)$/i";
			/*
			if (validate_email($_POST['user_email'],false)) {//邮箱是否合法
			}else{
				$data_return_array ['result'] = "0";
				$data_return_array ['msg'] = "メールアドレスが正しくありません。";
				$data_return_array ['data'] = "";
				$data_return = $this->JSON ( $data_return_array );
				die ( $data_return );
				exit ();
			}
			*/
			if ($_POST ['user_email']) {
				$user_info = $this->model->table ( "member" )->where ( "user_email = '" . $_POST['user_email'] . "' and user_id <> '" .  $user_id . "' " )->find ();
				if ($user_info) {//判断注册的账户是否存在
					$data_return_array ['result'] = "0";
					$data_return_array ['msg'] = "該当ユーザーが既に存在しました。";
					$data_return_array ['data'] = "";
					$data_return = $this->JSON ( $data_return_array );
					die ( $data_return );
					exit ();
				}else{
					$arr ['user_email'] = $_POST ['user_email'];
				}
			}
			if ($_POST ['user_nick']) {
				$user_info = $this->model->table ( "member" )->where ( "user_nick = '" . $_POST['user_nick'] . "' and user_id <> '" .  $user_id . "' " )->find ();
				if ($user_info) {//判断注册的账户是否存在
					$data_return_array ['result'] = "0";
					$data_return_array ['msg'] = "該当ユーザーが既に存在しました。";
					$data_return_array ['data'] = "";
					$data_return = $this->JSON ( $data_return_array );
					die ( $data_return );
					exit ();
				}else{
					$arr ['user_nick'] = $_POST ['user_nick'];
				}
			}
			if (isset($_POST ['user_sex'])) {
				$arr ['user_sex'] = $_POST ['user_sex'];
			}
			if (isset ( $_POST ['address'] )) {
				$arr ['address'] = $_POST ['address'];
			}
			if (isset ( $_POST ['industry'] )) {
				$arr ['industry'] = $_POST ['industry'];
			}
			if (isset ( $_POST ['size_company'] )) {
				$arr ['size_company'] = $_POST ['size_company'];
			}
			if (isset ( $_POST ['job'] )) {
				$arr ['job'] = $_POST ['job'];
			}
			if (isset ( $_POST ['reg_money'] )) {
				$arr ['reg_money'] = $_POST ['reg_money'];
			}
			if (isset ( $_POST ['calendar'] )) {
				$arr ['calendar'] = $_POST ['calendar'];
			}
			if (isset ( $_POST ['software'] )) {
				$arr ['software'] = $_POST ['software'];
			}
			if (isset ( $_POST ['hardware'] )) {
				$arr ['hardware'] = $_POST ['hardware'];
			}
			if (isset ( $_POST ['qualified'] )) {
				$arr ['qualified'] = $_POST ['qualified'];
			}
			if (isset ( $_POST ['allyear'] )) {
				$arr ['allyear'] = $_POST ['allyear'];
			}
			if (is_numeric ( $_POST ['mail_flg'] )) {
				$arr ['mail_flg'] = $_POST ['mail_flg'];
			}
			if ($_POST ['introduction']) {
				$arr ['introduction'] = $_POST ['introduction'];
			}
			//$arr ['insert_time'] = time ();
	//		$data = array_filter ( $arr );//这个会导致..传过来为空的时候会被屏蔽
	//		dump($arr );
			$data = $arr;
			$this->model->table ( 'member' )->data ( $data )->where ( "user_id = '" . $user_id . "' " )->update ();
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = "";
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	// 账户密码修改
	public function pwd_edit() {
		$user_id = $_POST ['user_id'];
		$old_pwd = $_POST ['old_pwd'];
		$user_pwd = $_POST ['user_pwd'];
		if (empty ( $user_id ) || empty ( $old_pwd ) || empty ( $user_pwd )) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$old_pwd = md5 ( $old_pwd );
		$user_info = $this->model->table ( 'member' )->where ( " user_id = '" . $user_id . "' and user_pwd = '" . $old_pwd . "' " )->find ();
		if (! $user_info) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "該当ユーザーは存在しない、またはパスワードが正しくありません";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		} else { // 如果用户存在，可以进行密码修改
			$data = array (
					"user_pwd" => md5 ( $user_pwd ) 
			);
			$this->model->table ( 'member' )->data ( $data )->where ( "user_id = '" . $user_id . "' " )->update ();
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = "";
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	
	// 判断密码是否正确
	public function pwd_check() {
		$user_id = $_POST ['user_id'];
		$old_pwd = $_POST ['old_pwd'];
		if (empty ( $user_id ) || empty ( $old_pwd )) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$old_pwd = md5 ( $old_pwd );
		$user_info = $this->model->table ( 'member' )->where ( " user_id = '" . $user_id . "' and user_pwd = '" . $old_pwd . "' " )->find ();
		if (! $user_info) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "該当ユーザーは存在しない、またはパスワードが正しくありません";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = "";
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	// 获取地址列表
	public function get_address() {
		$data_return_array = array ();
		$list = $this->model->table ( 'area' )->select ();
		$tmp = array ();
		if ($list) {
			foreach ( $list as $k => $vo ) {
				$tmp [$k] ['area_id'] = $vo ['area_id'];
				$tmp [$k] ['area_name'] = $vo ['area_name'];
				$tmp [$k] ['last_flg'] = 1;
			}
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	// 获取职务列表
	public function get_job() {
		$data_return_array = array ();
		$list = $this->model->table ( 'jobs' )->select ();
		$tmp = array ();
		if ($list) {
			foreach ( $list as $k => $vo ) {
				$tmp [$k] ['area_id'] = $vo ['job_id'];
				$tmp [$k] ['area_name'] = $vo ['name'];
				$tmp [$k] ['last_flg'] = 1;
			}
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	// 获取使用时间列表
	public function get_use() {
		$data_return_array = array ();
		$list = $this->model->table ( 'use' )->select ();
		$tmp = array ();
		if ($list) {
			foreach ( $list as $k => $vo ) {
				$tmp [$k] ['area_id'] = $vo ['use_id'];
				$tmp [$k] ['area_name'] = $vo ['time_name'];
				$tmp [$k] ['last_flg'] = 1;
			}
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	// 获取职业种类列表
	public function get_industry() {
		$data_return_array = array ();
		$list = $this->model->table ( 'industry' )->select ();
		$tmp = array ();
		if ($list) {
			foreach ( $list as $k => $vo ) {
				$tmp [$k] ['area_id'] = $vo ['industry_id'];
				$tmp [$k] ['area_name'] = $vo ['industry_name'];
				$tmp [$k] ['last_flg'] = 1;
			}
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	// 获取公司规模列表
	public function get_size_company() {
		$data_return_array = array ();
		$list = $this->model->table ( 'size_company' )->select ();
		$tmp = array ();
		if ($list) {
			foreach ( $list as $k => $vo ) {
				$tmp [$k] ['area_id'] = $vo ['size_company_id'];
				/*
				 * if ($vo['size_company_two'] !=''){
				 * $tmp [$k] ['area_name'] = $vo ['size_company_one']."~".$vo ['size_company_two']."人";
				 * }else{
				 * $tmp [$k] ['area_name'] = $vo ['size_company_one']."人 ~";
				 * }
				 */
				$tmp [$k] ['area_name'] = $vo ['size_company_name'];
				$tmp [$k] ['last_flg'] = 1;
			}
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	// 获取注册资金列表
	public function get_reg_money() {
		$data_return_array = array ();
		$list = $this->model->table ( 'reg_money' )->select ();
		$tmp = array ();
		if ($list) {
			foreach ( $list as $k => $vo ) {
				$tmp [$k] ['area_id'] = $vo ['reg_money_id'];
				$tmp [$k] ['area_name'] = $vo ['reg_money_name'];
				$tmp [$k] ['last_flg'] = 1;
			}
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	// 获取软件列表
	public function get_software($pid = null) {
		if (empty ( $pid )) {
			$pid = $_POST ['parent_id'];
		}
		if (empty ( $pid )) {
			$pid = 0;
		}
		$data_return_array = array ();
		$list = $this->model->table ( 'software' )->where ( "parent_id = " . $pid )->select ();
		$tmp = array ();
		if ($list) {
			foreach ( $list as $k => $vo ) {
				$tmp [$k] ['area_id'] = $vo ['software_id'];
				$tmp [$k] ['area_name'] = $vo ['software_name'];
				$tmp [$k] ['last_flg'] = $vo ['last_flg'];
			}
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	// 获取软件列表
	public function get_software_web() {
		$data_return_array = array ();
		$list = $this->model->table ( 'software' )->where ( "last_flg = 1")->select ();
		$tmp = array ();
		if ($list) {
			foreach ( $list as $k => $vo ) {
				$tmp [$k] ['area_id'] = $vo ['software_id'];
				$tmp [$k] ['area_name'] = $vo ['software_name'];
				$tmp [$k] ['last_flg'] = $vo ['last_flg'];
			}
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	// 获取硬件列表
	public function get_hardware($pid = null) {
		$data_return_array = array ();
		if (empty ( $pid )) {
			$pid = $_POST ['parent_id'];
		}
		if (empty ( $pid )) {
			$pid = 0;
		}
		$list = $this->model->table ( 'hardware' )->where ( "parent_id = " . $pid )->select ();
		$tmp = array ();
		if ($list) {
			foreach ( $list as $k => $vo ) {
				$tmp [$k] ['area_id'] = $vo ['hardware_id'];
				$tmp [$k] ['area_name'] = $vo ['hardware_name'];
				$tmp [$k] ['last_flg'] = $vo ['last_flg'];
			}
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	// 获取硬件列表
	public function get_hardware_web() {
		$data_return_array = array ();
		$list = $this->model->table ( 'hardware' )->where ( "last_flg = 1")->select ();
		$tmp = array ();
		if ($list) {
			foreach ( $list as $k => $vo ) {
				$tmp [$k] ['area_id'] = $vo ['hardware_id'];
				$tmp [$k] ['area_name'] = $vo ['hardware_name'];
				$tmp [$k] ['last_flg'] = $vo ['last_flg'];
			}
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	// 获取拥有资格列表
	public function get_qualified($pid = null) {
		$data_return_array = array ();
		if (empty ( $pid )) {
			$pid = $_POST ['parent_id'];
		}
		if (empty ( $pid )) {
			$pid = 0;
		}
		$list = $this->model->table ( 'qualified' )->where ( "parent_id = " . $pid )->select ();
		$tmp = array ();
		if ($list) {
			foreach ( $list as $k => $vo ) {
				$tmp [$k] ['area_id'] = $vo ['qualified_id'];
				$tmp [$k] ['area_name'] = $vo ['qualified_name'];
				$tmp [$k] ['last_flg'] = $vo ['last_flg'];
			}
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	// 获取拥有资格列表
	public function get_qualified_web() {
		$data_return_array = array ();
		$list = $this->model->table ( 'qualified' )->where ( "last_flg = 1")->select ();
		$tmp = array ();
		if ($list) {
			foreach ( $list as $k => $vo ) {
				$tmp [$k] ['area_id'] = $vo ['qualified_id'];
				$tmp [$k] ['area_name'] = $vo ['qualified_name'];
				$tmp [$k] ['last_flg'] = $vo ['last_flg'];
			}
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	// 获取全年预算列表
	public function get_allyear() {
		$data_return_array = array ();
		$list = $this->model->table ( 'allyear' )->select ();
		$tmp = array ();
		if ($list) {
			foreach ( $list as $k => $vo ) {
				$tmp [$k] ['area_id'] = $vo ['allyear_id'];
				$tmp [$k] ['area_name'] = $vo ['allyear_name'];
				$tmp [$k] ['last_flg'] = 1;
			}
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	// 获取个人信息(安卓专用)
	public function get_static_data() {
		$function = $_POST ['function'];
		
		if ($function == "get_address") {
			self::get_address ();
		}
		
		if ($function == "get_industry") {
			self::get_industry ();
		}
		
		if ($function == "get_size_company") {
			self::get_size_company ();
		}
		
		if ($function == "get_reg_money") {
			self::get_reg_money ();
		}
		
		if ($function == "get_software") {
			self::get_software ( $_POST ['parent_id'] );
		}
		
		if ($function == "get_hardware") {
			self::get_hardware ( $_POST ['parent_id'] );
		}
		
		if ($function == "get_qualified") {
			self::get_qualified ( $_POST ['parent_id'] );
		}
		
		if ($function == "get_allyear") {
			self::get_allyear ();
		}
		if ($function == "get_job") {
			self::get_job ();
		}
		if ($function == "get_use") {
			self::get_use ();
		}
	}
	public function push_open() {
		$user_id = $_POST ['user_id'];
		$push_flg = $_POST ['push_flg'];
		if (empty ( $user_id )) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$user_info = $this->model->table ( "member" )->where ( "user_id = '" . $user_id . "' " )->find ();
		if ($user_info) {
			if (empty ( $push_flg )) {
				$data_return_array ['result'] = "1";
				$data_return_array ['msg'] = "";
				$data_return_array ['data']['push_flg'] = $user_info ['push_flg'];
				$data_return = $this->JSON ( $data_return_array );
				die ( $data_return );
				exit ();
			} else {
				$data ['push_flg'] =$push_flg;
				$this->model->table ( "member" )->where ( "user_id = '" . $user_id . "' " )->data ( $data )->update ();
				$data_return_array ['result'] = "1";
				$data_return_array ['msg'] = "";
				$data_return_array ['data']['push_flg'] = $push_flg;
				$data_return = $this->JSON ( $data_return_array );
				die ( $data_return );
				exit ();
			}
		}
	}
}