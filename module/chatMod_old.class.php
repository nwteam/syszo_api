<?php
class chatMod extends commonMod {
	public function f_list() {
		$user_id = $_POST['user_id'];
		$p_num = $_POST['p_num'];
		$p_size = $_POST['p_size'];
		$tmp = array();
		if(empty($user_id)||empty($p_size)){
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		if(empty($p_num)){
			$p_num = "1";
		}
		$str = ($p_num - 1) * $p_size;
		$sql_a = "select count(distinct user_id) from app_fans where uid_a = '".$user_id."' ";
		$user_info_a = $this->model->query($sql_a);
		$uid_a_sum = $user_info_a[0]['count(distinct user_id)'];

		$sql_b = "select count(distinct user_id) from app_fans_b where uid_b = '".$user_id."' ";
		$user_info_b = $this->model->query($sql_b);
		$uid_b_sum = $user_info_b[0]['count(distinct user_id)'];

		$group_sum = $this->model->table('group')->where(" user_id = '".$user_id."' ")->count();
		$group = $this->model->table('group')->where(" user_id = '".$user_id."' ")->limit ( "$str" . "," . "$p_size" )->select();
		if($group){
			foreach ($group as $k => $v) {
				$member_sum = $this->model->table('group_member')->where(" group_id = '".$v['group_id']."' ")->count();
				$tmp['list_g'][$k]['group_name']      = $v['group_name'];
				$tmp['list_g'][$k]['group_number']    = $member_sum;
				$tmp['list_g'][$k]['group_id']        = $v['group_id'];

				$sql_mi = "select user_id from app_group_member where group_id = '".$v['group_id']."' ";
				$mem_info = $this->model->query($sql_mi);

				/*foreach ($mem_info as $kk=>$vv){
					$tmp['list_g'][$k]['gm'][$kk]['gm_id'] = $vv['user_id'];
					$sql_mn = "select user_nick from app_member where user_id = '".$vv['user_id']."' ";
					$user_nick = $this->model->query($sql_mn);
					foreach ($user_nick as $kn => $vn){
					$tmp['list_g'][$k]['gm'][$kk]['gm_name'] = $vn['user_nick'];
					}
					unset($user_nick);
					}*/

				$tmp['list_g'][$k]['group_img']       = $v['group_img'];
				unset($member_sum);
			}
		}
		$sql = "SELECT user_id FROM app_fans where uid_a = '".$user_id."'  GROUP By user_id limit $str,$p_size";
		$member_info = $this->model->query($sql);
		if($member_info){
			foreach ($member_info as $key => $value) {
				$member_name = $this->model->table('member')->where(" user_id = '".$value['user_id']."' ")->find();
				$tmp['list_m'][$key]['member_name'] = $member_name['user_nick'];
				$tmp['list_m'][$key]['member_id'] = $value['user_id'];
				unset($member_name);
			}
		}
		$tmp['uid_a_sum'] = $uid_a_sum;
		$tmp['uid_b_sum'] = $uid_b_sum;
		$tmp['group_sum'] = $group_sum;
		$tmp['member_sum'] = $uid_a_sum;
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	public function group_read(){
		$group_id = $_POST['group_id'];
		$p_num = $_POST['p_num'];
		$p_size = $_POST['p_size'];
		if(empty($group_id)||empty($p_size)){
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		if(empty($p_num)){
			$p_num = "1";
		}
		$str = ($p_num - 1) * $p_size;
		$group_info = $this->model->table("group_member")->where("group_id = $group_id")->limit ( "$str" . "," . "$p_size" )->select();
		if($group_info){
			foreach ($group_info as $key => $val){
				$tmp[$key]['member_id'] = $val['user_id'];
				$sql_mn = "select user_nick from app_member where user_id = '".$val['user_id']."' ";
				$user_nick = $this->model->query($sql_mn);
				foreach ($user_nick as $k=>$v){
					$tmp[$key]['member_name'] = $v['user_nick'];
				}
				unset($user_nick);
			}
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	public function a_list() {
		$user_id 	= $_POST['user_id'];
		$p_num 		= $_POST['p_num'];
		$p_size 	= $_POST['p_size'];
		$tmp = array();
		if(empty($user_id)||empty($p_size)){
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		if(empty($p_num)){
			$p_num = "1";
		}
		$str = ($p_num - 1) * $p_size;
		$sql = "SELECT user_id FROM app_fans where uid_a = '".$user_id."'  GROUP By user_id limit $str,$p_size";
		$member_info = $this->model->query($sql);
		//$tmp_data = array();
		if($member_info){
			//$i = 0;
			foreach ($member_info as $key => $value) {
				//$i++;
				$member_name = $this->model->table('member')->where(" user_id = '".$value['user_id']."' ")->find();
				$tmp[$key]['member_name'] = $member_name['user_nick'];
				$tmp[$key]['member_id']   = $value['user_id'];
				unset($member_name);
			}
			//$tmp['member_sum'] = $i;
			//$tmp['add'] = $tmp_data;
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	public function b_list() {
		$user_id = $_POST['user_id'];
		$p_num = $_POST['p_num'];
		$p_size = $_POST['p_size'];
		$tmp = array();
		if(empty($user_id)||empty($p_size)){
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		if(empty($p_num)){
			$p_num = "1";
		}
		$str = ($p_num - 1) * $p_size;
		$sql = "SELECT user_id FROM app_fans_b where uid_b = '".$user_id."'  GROUP By uid_b limit $str,$p_size";
		$member_info = $this->model->query($sql);
		if($member_info){
			foreach ($member_info as $key => $value) {
				$member_name = $this->model->table('member')->where(" user_id = '".$value['user_id']."' ")->find();
				$tmp[$key]['member_name'] = $member_name['user_nick'];
				$tmp[$key]['member_id'] = $value['user_id'];
				$if_member = $this->model->table('fans')->where(" user_id = '".$value['user_id']."' ")->find();
				if($if_member){
					$tmp[$key]['if_member'] = "1";
				}else{
					$tmp[$key]['if_member'] = "2";
				}
				unset($member_name);
				unset($if_member);
			}
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	public function add_f(){
		$user_id = $_POST['user_id'];
		$rec_id  = $_POST['rec_id'];
		if(empty($user_id)||empty($rec_id)){
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$data = array(
			"uid_a" 		=> $user_id,
			"user_id"   	=> $rec_id,
			"insert_time"	=> time()
			
		);
		$this->model->table("fans")->data($data)->insert();
		$data_b = array(
			"user_id" 		=> $user_id,
			"uid_b"   		=> $rec_id,
			"insert_time"	=> time()
		);
		$this->model->table("fans_b")->data($data_b)->insert();
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	public function del_f(){
		$user_id = $_POST['user_id'];
		$rec_id  = $_POST['rec_id'];
		if(empty($user_id)||empty($rec_id)){
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		
		$this->model->table("fans")->where(" uid_a = $user_id and user_id = $rec_id ")->delete();
		
		$this->model->table("fans_b")->where(" user_id = $user_id and uid_b = $rec_id ")->delete();
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	public function check() {
		$user_id = $_POST['user_id'];
		$p_num = $_POST['p_num'];
		$p_size = $_POST['p_size'];
		$user_nick = $_POST['user_nick'];
		$tmp = array();
		if(empty($p_size)){
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		if(empty($p_num)){
			$p_num = "1";
		}
		$str = ($p_num - 1) * $p_size;
		if($user_id&&$user_nick){
			$sql = "SELECT * FROM app_member WHERE user_nick LIKE '%{$user_nick}%' AND user_id LIKE '{$user_id}' limit $str,$p_size ";//名称和ID搜索
		}elseif($user_nick){
			$sql = "SELECT * FROM app_member WHERE user_nick LIKE '%{$user_nick}%' limit $str,$p_size ";//名称搜索
		}elseif($user_id){
			$sql = "SELECT * FROM app_member WHERE user_id LIKE '{$user_id}' limit $str,$p_size ";//ID搜索
		}
		$check_name = $this->model->query($sql);
		if(!$check_name){
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "検索結果がありません";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		//$i=0;
		foreach ($check_name as $key => $value) {
			//$i++;
			$tmp[$key]['member_name']   = $value['user_nick'];
			$tmp[$key]['member_id']     = $value['user_id'];
		}
		//$tmp['check_num'] = $i;
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	//分类检索结果画面
	public function cat(){
		$cat_type = $_POST['cat_type'];
		if(empty($cat_type)){
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		if($cat_type == "1"){
			$cat_info = $this->model->table("cat_company")->select();
			foreach($cat_info as $k => $v){
				$tmp[$k]['cat_name'] = $v['company_number'];
				$tmp[$k]['cat_id']   = $v['company_id'];
			}
		}elseif($cat_type == "2"){
			$cat_info = $this->model->table("jobs")->select();
			foreach($cat_info as $k => $v){
				$tmp[$k]['cat_name'] = $v['name'];
				$tmp[$k]['cat_id']   = $v['job_id'];
			}
		}elseif($cat_type == "3"){
			$cat_info = $this->model->table("hardware")->select();
			foreach($cat_info as $k => $v){
				$tmp[$k]['cat_name'] = $v['hardware_name'];
				$tmp[$k]['cat_id']   = $v['hardware_id'];
			}
		}elseif($cat_type == "4"){
			$cat_info = $this->model->table("software")->select();
			foreach($cat_info as $k => $v){
				$tmp[$k]['cat_name'] = $v['software_name'];
				$tmp[$k]['cat_id']   = $v['software_id'];
			}
		}elseif($cat_type == "5"){
			$cat_info = $this->model->table("qualified")->select();
			foreach($cat_info as $k => $v){
				$tmp[$k]['cat_name'] = $v['qualified_name'];
				$tmp[$k]['cat_id']   = $v['qualified_id'];
			}
		}elseif($cat_type == "6"){
			$cat_info = $this->model->table("use")->select();
			foreach($cat_info as $k => $v){
				$tmp[$k]['cat_name'] = $v['time_name'];
				$tmp[$k]['cat_id']   = $v['use_id'];
			}
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	//公司规模检索结果画面
	public function cat_search(){
		$cat_sonType = $_POST['cat_sonType'];
		$p_num = $_POST['p_num'];
		$p_size = $_POST['p_size'];
		$user_id = $_POST['user_id'];
		if(empty($cat_sonType)||empty($p_size)||empty($user_id)){
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		if(empty($p_num)){
			$p_num = "1";
		}
		$str = ($p_num - 1) * $p_size;

		if($cat_sonType == "1"){
			$where = "size_company = 1 ";
		}elseif($cat_sonType == "2"){
			$where = "size_company = 2 ";
		}elseif($cat_sonType == "3"){
			$where = "size_company = 3 ";
		}elseif($cat_sonType == "4"){
			$where = "size_company = 4 ";
		}elseif($cat_sonType == "5"){
			$where = "size_company = 5 ";
		}elseif($cat_sonType == "6"){
			$where = "size_company = 6 ";
		}
		$sql = "select * from app_member where {$where}  limit $str,$p_size  ";
		$company_info = $this->model->query($sql);
		if(!$company_info){
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "検索結果がありません";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		foreach ($company_info as $k => $v) {
			//                $i++;
			$tmp[$k]['member_name'] = $v['user_nick'];
			$tmp[$k]['member_id'] = $v['user_id'];
			$if_member = $this->model->table('fans')->where(" uid_a = '".$user_id."' and user_id = '".$v['user_id']."' ")->find();
			if($if_member){
				$tmp[$k]['if_member'] = "1";
			}else{
				$tmp[$k]['if_member'] = "2";
			}
		}
		//            $tmp['check_num'] = $i;
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}

	//职务检索结果画面
	public function job_search(){
		$cat_sonType = $_POST['cat_sonType'];
		$p_num = $_POST['p_num'];
		$p_size = $_POST['p_size'];
		if(empty($cat_sonType)||empty($p_size)){
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		if(empty($p_num)){
			$p_num = "1";
		}
		$str = ($p_num - 1) * $p_size;
		if($cat_sonType == "1"){
			$where = "job = 1 ";
		}elseif($cat_sonType == "2"){
			$where = "job = 2 ";
		}elseif($cat_sonType == "3"){
			$where = "job = 3 ";
		}elseif($cat_sonType == "4"){
			$where = "job = 4 ";
		}elseif($cat_sonType == "5"){
			$where = "job = 5 ";
		}elseif($cat_sonType == "6"){
			$where = "job = 6 ";
		}elseif($cat_sonType == "7"){
			$where = "job = 7 ";
		}elseif($cat_sonType == "8"){
			$where = "job = 8 ";
		}
		$sql = "select * from app_member where {$where}  limit $str,$p_size  ";
		$company_info = $this->model->query($sql);
		if(!$company_info){
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "検索結果がありません";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		foreach ($company_info as $k => $v) {
			//                $i++;
			$tmp[$k]['member_name'] = $v['user_nick'];
			$tmp[$k]['member_id'] = $v['user_id'];
		}
		//            $tmp['check_num'] = $i;
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	//软件检索结果画面
	public function software_search(){
		$cat_sonType = $_POST['cat_sonType'];
		$p_num = $_POST['p_num'];
		$p_size = $_POST['p_size'];
		if(empty($cat_sonType)||empty($p_size)){
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		if(empty($p_num)){
			$p_num = "1";
		}
		$str = ($p_num - 1) * $p_size;
		if($cat_sonType == "1"){
			$where = "job = 1 ";
		}elseif($cat_sonType == "2"){
			$where = "job = 2 ";
		}elseif($cat_sonType == "3"){
			$where = "job = 3 ";
		}elseif($cat_sonType == "4"){
			$where = "job = 4 ";
		}
		$sql = "select * from app_member where {$where}  limit $str,$p_size  ";
		$company_info = $this->model->query($sql);
		if(!$company_info){
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "検索結果がありません";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		foreach ($company_info as $k => $v) {
			$tmp[$k]['member_name'] = $v['user_nick'];
			$tmp[$k]['member_id'] = $v['user_id'];
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	//硬件检索结果画面
	public function hardware_search(){
		$cat_sonType = $_POST['cat_sonType'];
		$p_num = $_POST['p_num'];
		$p_size = $_POST['p_size'];
		if(empty($cat_sonType)||empty($p_size)){
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		if(empty($p_num)){
			$p_num = "1";
		}
		$str = ($p_num - 1) * $p_size;
		if($cat_sonType == "1"){
			$where = "hardware = 1 ";
		}elseif($cat_sonType == "2"){
			$where = "hardware = 2 ";
		}elseif($cat_sonType == "3"){
			$where = "hardware = 3 ";
		}elseif($cat_sonType == "4"){
			$where = "hardware = 4 ";
		}
		$sql = "select * from app_member where {$where}  limit $str,$p_size  ";
		$company_info = $this->model->query($sql);
		if(!$company_info){
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "検索結果がありません";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		foreach ($company_info as $k => $v) {
			$tmp[$k]['member_name'] = $v['user_nick'];
			$tmp[$k]['member_id'] = $v['user_id'];
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}

	//拥有资格检索结果画面
	public function qualified_search(){
		$cat_sonType = $_POST['cat_sonType'];
		$p_num = $_POST['p_num'];
		$p_size = $_POST['p_size'];
		if(empty($cat_sonType)||empty($p_size)){
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		if(empty($p_num)){
			$p_num = "1";
		}
		$str = ($p_num - 1) * $p_size;
		if($cat_sonType == "1"){
			$where = "qualified = 1 ";
		}elseif($cat_sonType == "2"){
			$where = "qualified = 2 ";
		}elseif($cat_sonType == "3"){
			$where = "qualified = 3 ";
		}elseif($cat_sonType == "4"){
			$where = "qualified = 4 ";
		}elseif($cat_sonType == "5"){
			$where = "qualified = 5 ";
		}
		$sql = "select * from app_member where {$where}  limit $str,$p_size  ";
		$company_info = $this->model->query($sql);
		if(!$company_info){
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "検索結果がありません";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		foreach ($company_info as $k => $v) {
			$tmp[$k]['member_name'] = $v['user_nick'];
			$tmp[$k]['member_id'] = $v['user_id'];
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	//使用时间检索结果画面
	public function use_search(){
		$cat_sonType = $_POST['cat_sonType'];
		$p_num = $_POST['p_num'];
		$p_size = $_POST['p_size'];
		if(empty($cat_sonType)||empty($p_size)){
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		if(empty($p_num)){
			$p_num = "1";
		}
		$str = ($p_num - 1) * $p_size;
		if($cat_sonType == "1"){
			$where = "calendar = 1 ";
		}elseif($cat_sonType == "2"){
			$where = "calendar = 2 ";
		}elseif($cat_sonType == "3"){
			$where = "calendar = 3 ";
		}elseif($cat_sonType == "4"){
			$where = "calendar = 4 ";
		}elseif($cat_sonType == "5"){
			$where = "calendar = 5 ";
		}elseif($cat_sonType == "6"){
			$where = "calendar = 6 ";
		}elseif($cat_sonType == "7"){
			$where = "calendar = 7 ";
		}
		$sql = "select * from app_member where {$where}  limit $str,$p_size  ";
		$company_info = $this->model->query($sql);
		if(!$company_info){
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "検索結果がありません";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		foreach ($company_info as $k => $v) {
			$tmp[$k]['member_name'] = $v['user_nick'];
			$tmp[$k]['member_id'] = $v['user_id'];
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}

	//朋友资料画面
	public function fdata() {
		$user_id = $_POST['user_id'];
		$member_id = $_POST['member_id'];
		if(empty($user_id)||empty($member_id)){
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$user_info = $this->model->table ( "member" )->where ( "user_id = '" . $member_id . "' " )->find ();
		//print_r($user_info);exit;
		if(!$user_info){
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "該当ユーザーがありません";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$tmp['user_nick']    =	$user_info['user_nick'];
		$tmp['user_sex']     =	$user_info['user_sex'];
		$address       =  	 $this->model->table('area')->where("area_id = '".$user_info['address']."' ")->find ();
		$tmp['address']      =	$address['area_name'];//地址

		$industry      =     $this->model->table('industry')->where("industry_id = '".$user_info['industry']."' ")->find ();
		$tmp['industry']     =	$industry['industry_name'];//职业种类
		
		$size_company  = 	$this->model->table('size_company')->where("size_company_id = '".$user_info['size_company']."' ")->find ();
		$tmp['size_company'] = 	$size_company['size_company_name'];//公司规模
		
        $jobs          = 	$this->model->table('jobs')->where("job_id = '".$user_info['job']."' ")->find ();
		$tmp['job']  	     =	$jobs['name'];//职务
		
		$reg_money     =    $this->model->table('reg_money')->where("reg_money_id = '".$user_info['reg_money']."' ")->find ();
		$tmp['reg_money']    = 	$reg_money['reg_money_name'];//注册资金
		
        $calendar      =    $this->model->table('use')->where("use_id = '".$user_info['calendar']."' ")->find ();
		$tmp['calendar']     = 	$calendar['time_name'];//使用时间
		
		$software      =    $this->model->table('software')->where("software_id = '".$user_info['software']."' ")->find ();
		$tmp['software']     = 	$software['software_name'];//软件
		
		$hardware      =    $this->model->table('hardware')->where("hardware_id = '".$user_info['hardware']."' ")->find ();
		$tmp['hardware']     = 	$hardware['hardware_name'];//硬件
		
		$qualified     =    $this->model->table('qualified')->where("qualified_id = '".$user_info['qualified']."' ")->find ();
		$tmp['qualified']    = 	$qualified['qualified_name'];//拥有资格
		
		$allyear       =    $this->model->table('allyear')->where("allyear_id = '".$user_info['allyear']."' ")->find ();
		$tmp['allyear']      = 	$allyear['allyear_name'];//全年预算
		
		$number        =    $this->model->table('fans')->where("uid_a = '".$user_id."' ")->count();
		$tmp['number']       = 	$number;//我的好友
		
		$price         =    $this->model->table('good')->where("user_id = $user_id ")->count();
		$tmp['price']        = 	$price;//我的点赞数
		
		if($user_info['insert_time']){
		$tmp['time']         =  date("Y-m-d",$user_info['insert_time']);
		}
		$tmp['introduction'] = 	$user_info['introduction'];
		
		$if_member = $this->model->table('fans')->where(" uid_a = '".$user_id."' and user_id = '".$member_id."' ")->find();
		if($if_member){
			$tmp['if_member'] = "1";
		}else{
			$tmp['if_member'] = "2";
		}
		$tmp['user_email']   =  $user_info['user_email'];
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	public function group_add(){
		$group_name   = $_POST['group_name'];
		$group_img    = $_POST['group_img'];
		$user_id      = $_POST['user_id'];
		$member_id    = $_POST['member_id'];
		if(empty($group_name)||empty($user_id)){
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$data = array(
                "group_name" => $group_name,
                "group_img"  => $group_img,
                "user_id"    => $user_id
		);
		$group_id = $this->model->table('group')->data($data)->insert();
		$data_d = array(
                        "group_id"      => $group_id,
                        "user_id"       => $user_id,
                        "insert_time"   => time()
		);
		$this->model->table('group_member')->data($data_d)->insert();
		//echo $group_id;exit;
		if(!$group_id){
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "作成を失敗しました。";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}

		$arr = json_decode($member_id);
		if($arr){
			foreach ($arr as $k => $v) {
				$data_m = array(
                        "group_id"      => $group_id,
                        "user_id"       => $v,
                        "insert_time"   => time()
				);
				$this->model->table('group_member')->data($data_m)->insert();
				$member_name = $this->model->table('member')->where("user_id = $v")->find();
				$tmp[$k]['member_name'] = $member_name['user_nick'];
			}
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
		//$group_info = $this->model->table('group_member')->where("group_id = '".$group_id."' ")->select();
	}
	public function member_add(){
		//            $group_id = $_POST['group_id'];
		$user_id  = $_POST['user_id'];
		if(empty($user_id)){
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$user_info = $this->model->table('fans')->where("uid_a = '".$user_id."' ")->select();
		if($user_info){
			foreach ($user_info as $key => $value) {
				$member_name = $this->model->table('member')->where("user_id = '".$value['user_id']."' ")->find();
				$tmp[$key]['member_name'] = $member_name['user_nick'];
				$tmp[$key]['member_id']   = $member_name['user_id'];
				//                    $if_add = $this->model->table('group_member')->where("group_id = '".$group_id."' and user_id = '".$value['uid_a']."' ")->find();
				//                    if($if_add){
				//                        $tmp[$key]['if_add'] = "1";
				//                    }else{
				//                        $tmp[$key]['if_add'] = "2";
				//                    }
				unset($member_name);
			}
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	public function group_d(){
		$group_id = $_POST['group_id'];
		if(empty($group_id)){
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$group_info = $this->model->table('group')->where("group_id = $group_id")->find();
		$sql = "select * from app_group_member where group_id = {$group_id}";
		$member_info = $this->model->query($sql);
		$tmp_data = array();
		if($member_info){
			foreach ($member_info as $key => $value) {
				$sql = "select user_nick,user_id from app_member where user_id = {$value['user_id']} ";
				$name = $this->model->query($sql);
				$tmp_data[$key]['member_id'] = $name[0]['user_id'];
				$tmp_data[$key]['member_name'] = $name[0]['user_nick'];
				unset($name);
			}
		}
		$tmp['group_img']  = $group_info['group_img'];
		$tmp['group_name'] = $group_info['group_name'];
		$tmp['group_data'] = $tmp_data;
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	public function group_edit(){
		$group_id     = $_POST['group_id'];
		$group_name   = $_POST['group_name'];
		$user_id      = $_POST['user_id'];
		$group_img    = $_POST['group_img'];
		$aid_type    = $_POST['aid_type'];//添加群成员
		$did_type    = $_POST['did_type'];//删除群成员
		$tmp = array();
		if(empty($user_id)||empty($group_id)){
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$data = array(
                "group_name" => $group_name,
                "group_img"  => $group_img,
                "insert_time"=> time()
		);
		$this->model->table('group')->where("user_id = $user_id and group_id = $group_id")->data($data)->update();
		$group_info = $this->model->table('group_member')->where("group_id = $group_id")->select();
		//print_r($group_info);exit;
		$aid = json_decode($aid_type);//选择的添加的成员
		if($aid){
			foreach ($aid as $k => $v) {
				$data_a = array(
                            "group_id"      => $group_id,
                            "user_id"       => $v,
                            "insert_time"   => time()
				);
				$this->model->table('group_member')->data($data_a)->insert();
			}
		}
		$did = json_decode($did_type);//选择的删除的成员
		if($did){
			foreach ($did as $k => $v) {
				$this->model->table('group_member')->where("user_id = $v and group_id = $group_id")->delete();
			}
		}

		if($group_info){
			foreach ($group_info as $key => $value) {
				$abc = $this->model->table('member')->where("user_id = '".$value['user_id']."' ")->select();
				if($abc){
					foreach ($abc as $kel => $vel) {
						$tmp[$key]['member_name'] = $vel['user_nick'];
						$tmp[$key]['member_id'] = $vel['user_id'];
					}
				}
			}
		}
		if(!$group_info){
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "修改を失敗しました。";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}

	public function group_edit_1(){
		$group_id     = $_POST['group_id'];
		$group_name   = $_POST['group_name'];
		$user_id      = $_POST['user_id'];
		$group_img    = $_POST['group_img'];
		$aid_type     = $_POST['aid_type'];//群成员
		$tmp = array();
		if(empty($user_id)||empty($group_id)){
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$data = array(
                "group_name" => $group_name,
                "group_img"  => $group_img,
                "insert_time"=> time()
		);
		$this->model->table('group')->where("user_id = $user_id and group_id = $group_id")->data($data)->update();
		$group_info = $this->model->table('group_member')->where("group_id = $group_id")->select();
		//print_r($group_info);exit;
		$aid = json_decode($aid_type);//选择的添加或删除的成员id结果
		//print_r($aid);exit;
		if($aid){
				
			$this->model->table(group_member)->where("group_id = $group_id")->delete();
				
			foreach ($aid as $k => $v) {
				$data_a = array(
                            "group_id"      => $group_id,
                            "user_id"       => $v,
                            "insert_time"   => time()
				);
				$this->model->table('group_member')->data($data_a)->insert();
			}
		}

		if($aid){
			foreach ($aid as $key => $value) {
				$abc = $this->model->table('member')->where("user_id = '".$value."' ")->select();
				if($abc){
					foreach ($abc as $kel => $vel) {
						$tmp[$key]['member_name'] = $vel['user_nick'];
						$tmp[$key]['member_id'] = $vel['user_id'];
					}
				}
			}
		}
		if(!$group_info){
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "修改を失敗しました。";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}

	public function group_del(){
		$member_id      = $_POST['member_id'];
		$group_id     = $_POST['group_id'];
		if(empty($member_id)||empty($group_id)){
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$group_info = $this->model->table('group')->where("group_id = $group_id AND user_id = $member_id ")->find();
		if($group_info){
			$this->model->table('group')->where("group_id = $group_id AND user_id = $member_id ")->delete();
			$this->model->table('group_member')->where("group_id = $group_id ")->delete();
		}else{
			$this->model->table('group_member')->where("group_id = $group_id AND user_id = $member_id ")->delete();
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = "";
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	public function send_info(){
		$user_id        = $_POST['user_id'];
		$group_id       = $_POST['group_id'];
		$rec_id      	= $_POST['rec_id'];
		$info_content   = $_POST['info_content'];
		$info_img       = $_POST['info_img'];
		if(!empty($rec_id)){
			if(empty($rec_id)||empty($user_id)){
				$data_return_array ['result'] = "0";
				$data_return_array ['msg'] = "引数が足りない";
				$data_return_array ['data'] = "";
				$data_return = $this->JSON ( $data_return_array );
				die ( $data_return );
				exit ();
			}
		}elseif(!empty($group_id)){
			if(empty($group_id)||empty($user_id)){
				$data_return_array ['result'] = "0";
				$data_return_array ['msg'] = "引数が足りない";
				$data_return_array ['data'] = "";
				$data_return = $this->JSON ( $data_return_array );
				die ( $data_return );
				exit ();
			}
		}
		if($group_id){
			$data = array(
				"group_id"		=> $group_id,
                "user_id"       => $user_id,
                "info_img"      => $info_img,
                "info_content"  => $info_content,
                "insert_time"   => time()
			);
		}else{
			$data = array(
                "user_id"       => $user_id,
				"rec_id"		=> $rec_id,
                "info_img"      => $info_img,
                "info_content"  => $info_content,
                "insert_time"   => time()
			);
		}
		$info_id = $this->model->table("group_info")->data($data)->insert();
		//------------添加代码----------------
		if(!$group_id){
			$new_info = $this->model->table("new_info")->where(" (user_id = $user_id and rec_id = $rec_id) or (user_id = $rec_id and rec_id = $user_id) ")->find();
				
			$data_new = array(
	                "user_id"       => $user_id,
					"rec_id"		=> $rec_id,
	                "info_img"      => $info_img,
	                "info_content"  => $info_content,
	                "insert_time"   => time()
			);
			if($new_info){
				$this->model->table("new_info")->where(" (user_id = $user_id and rec_id = $rec_id) or (user_id = $rec_id and rec_id = $user_id) ")->data($data_new)->update();
			}elseif (!$new_info){
				$this->model->table("new_info")->data($data_new)->insert();
			}
		}
		//------------添加代码----------------
		if($user_id == $data['user_id']){
			$if_me = "2";
		}else{
			$if_me = "1";
		}
		if(!empty($group_id)){
			$if_group = "1";
			$uid_list = $this->model->table("group_member")->where(" group_id = '".$group_id."' ")->select();
				
			foreach ($uid_list as $k=>$v){
				if ($v['user_id'] != $user_id){
					$user =  module ( "push" )->user_info($v['user_id']);
					//print_r($user);
					if ($user['device_id']){
						$push_data = array(
							"info_type" 	=> $if_group,
							"info_content"  => $info_content,
							"info_img"      => $info_img,
							"send_time"		=> date("H:i",time()),
							//"if_me"			=> "2",
							"user_id"	    => $user_id
						);
						module ( "push" )->push ( "新しいメッセージ！", $push_data, $user['device_id'], $user['device']);
					}
				}
			}

		}else{
			$if_group = "2";
			$user =  module ( "push" )->user_info($rec_id);
			if ($user['device_id']){
				$push_data = array(
					"info_type"    => $if_group,
					"info_content" => $info_content,
					"info_img"     => $info_img,
					"send_time"	   => date("H:i",time()),
					//"if_me"		   => "2",
					"user_id"	   => $user_id
				);
				module ( "push" )->push ( "新しいメッセージ！", $push_data, $user['device_id'], $user['device'] );
			}
		}
		//$tmp['if_me'] = $if_me;
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	public function info_list(){
		$group_id  = $_POST['group_id'];
		$user_id   = $_POST['user_id'];
		$rec_id    = $_POST['rec_id'];
		$p_num 	   = $_POST['p_num'];
		$p_size    = $_POST['p_size'];
		if(empty($user_id)||empty($p_size)){
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		if(empty($p_num)){
			$p_num = "1";
		}
		$str = ($p_num - 1) * $p_size;
		if($group_id){
			//------------群聊---------------
			$group = "1";
			$sql = "select * from app_group_info
					where group_id = $group_id 
					order by insert_time asc 
					limit $str,$p_size";
			$group_member = $this->model->table("group_member")->where("user_id = $user_id")->find();
			if($group_member){
				$group_read_info = $this->model->table("group_info")->where("group_id = $group_id")->select();
				if($group_read_info){
					foreach ($group_read_info as $key => $val){
						if(empty($val['if_read_group_uids'])){
							$data = array(
								"if_read_group_uids" => $user_id
							);
						}elseif(!empty($val['if_read_group_uids'])){
							$gread_id = $val['if_read_group_uids'].",".$user_id;
							$data = array(
								"if_read_group_uids" => $gread_id
							);
						}
						$this->model->table("group_info")->where(" group_id = $group_id ")->data($data)->order("insert_time desc")->update();
					}
				}
			}else{
				$data_return_array ['result'] = "0";
				$data_return_array ['msg'] = "権限がありません";
				$data_return_array ['data'] = "";
				$data_return = $this->JSON ( $data_return_array );
				die ( $data_return );
				exit ();
			}
					
		}else{
			//------------单聊---------------
			$group = "2";
			$sql = "select * from app_group_info
					where group_id = 0 and (user_id = $user_id and rec_id = $rec_id) or (user_id = $rec_id and rec_id = $user_id)  
					order by insert_time asc 
					limit $str,$p_size";
			$data = array(
					"if_read" => "2"
					);
						
					$this->model->table("group_info")->where(" group_id = 0 and (user_id = $user_id and rec_id = $rec_id) or (user_id = $rec_id and rec_id = $user_id) ")->data($data)->order("insert_time desc")->update();
					$this->model->table("new_info")->where("(user_id = $user_id and rec_id = $rec_id) or (user_id = $rec_id and rec_id = $user_id) ")->data($data)->update();
		}

		$info = $this->model->query($sql);
		$tmp_data = array();
		if($info){
			foreach ($info as $key => $val){
				$sql_n = "select user_nick from app_member where user_id = '".$val['user_id']."' ";
				$user_name = $this->model->query($sql_n);
				foreach ($user_name as $k => $v){
					$tmp_data[$key]['uname'] = $v['user_nick'];
				}
				$tmp_data[$key]['info_content'] = $val['info_content'];
				$tmp_data[$key]['info_img']     = $val['info_img'];
				$tmp_data[$key]['info_time']	= date("H:i",$val['insert_time']);
				$tmp_data[$key]['user_id']      = $val['user_id'];
				/*if($user_id == $val['user_id']){
					$tmp_data[$key]['if_me'] = "1";
				}else{
					$tmp_data[$key]['if_me'] = "2";
				}*/
				unset($user_name);
			}
		}

		$tmp['if_group'] = $group;
		//$tmp['if_me']    = $if_me;
		$tmp['group_info'] = $tmp_data;

		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}

	public function test(){
		$push_data = array(
							"info_type" 	=> $_POST['msg_type'],
							"info_content"  => $_POST['content'],
							"info_img"      => $_POST['img'],
							"send_time"		=> date("H:i",time()),
							"if_me"			=> 2,
							"user_id"		=> $_POST['uid']
		);
		module ( "push" )->push ( "新しいメッセージ！", $push_data, $_POST['device_id'], $_POST['device_type']);
	}
}














