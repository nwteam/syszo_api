<?php
class messageMod extends commonMod {
	public function look() {
		$user_id  = $_POST['user_id'];
		$today = strtotime("today");
		$tomorrow = strtotime("today")+3600*24;
//		$p_num = $_POST['p_num'];
//		$p_size = $_POST['p_size'];
		if (empty ( $user_id ) ) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "引数が足りない";
			$data_return_array ['data'] = "";
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		if(empty($p_num)){
			$p_num = "1";
		}
		$str = ($p_num - 1) * $p_size;
		
		$group_member = $this->model->table("group_member")->where("user_id = '".$user_id."' ")->select();
		$group_ids = array();
		if ($group_member){
			foreach ($group_member as $key => $val){
				$group_ids[] = $val['group_id'];
			}
		}
		if ($group_ids){
			$group_ids = implode(",", $group_ids);
			$sql = "SELECT * FROM app_group_info
            		where group_id in (".$group_ids.")
            		GROUP By group_id 
            		ORDER BY insert_time desc ";
			$group_info = $this->model->query($sql);
		}else {
			$group_info = array();
		}
		//var_dump($group_info);

		if($group_info){
			foreach ($group_info as $key => $value) {
				$group = $this->model->table('group_info')->where("group_id = '".$value['group_id']."' and if_read = 2")->order("insert_time desc")->find();
				//print_r($group);
				$tmp['glist'][$key]['group_id']       = $group['group_id'];
				$member_name = $this->model->table("group")->where("group_id = '".$value['group_id']."' ")->find();
				$tmp['glist'][$key]['group_name']     = $member_name['group_name'];
				if($group['info_img']){
					$tmp['glist'][$key]['ginfo_img']      = "[イメージメッセージ]";
				}
				$tmp['glist'][$key]['ginfo_content']  = $group['info_content'];
				if($group['insert_time']>=$today&&$group['insert_time']<=$tomorrow){
					$tmp['glist'][$key]['insert_time']    = date("H:i",$group['insert_time']);
				}else{
					$tmp['glist'][$key]['insert_time']    = date("m-d",$group['insert_time']);
				}
					
				$group_sum = $this->model->table('group_member')->where("group_id = '".$value['group_id']."' ")->count();
				$tmp['glist'][$key]['ginfo_sum']      = $group_sum;
				unset($group);
				unset($member_name);
				unset($group_sum);
			}
		}
		$sql_m = "SELECT * FROM app_new_info
            	      where user_id = {$user_id} or rec_id = {$user_id} 
            	      
            	      ORDER BY insert_time desc ";
		//GROUP By rec_id 
		$member_info = $this->model->query($sql_m);
		//print_r($member_info);exit;
		//var_dump($member_info);
		if($member_info){
			foreach ($member_info as $k => $v) {
				if($user_id == $v['user_id']){
					$tmp['mlist'][$k]['user_id'] 	= $v['rec_id'];
				}else{
					$tmp['mlist'][$k]['user_id'] 	= $v['user_id'];
				}


				$user_name = $this->model->table("member")->where("user_id = '".$v['user_id']."' ")->find();
				if($user_name){
					$tmp['mlist'][$k]['user_name']  = $user_name['user_nick'];
				}
				if($v['info_img']){
					$tmp['mlist'][$k]['minfo_img']      = "[イメージメッセージ]";
				}
				$tmp['mlist'][$k]['minfo_content']  = $v['info_content'];
				if($v['insert_time']>=$today&&$v['insert_time']<=$tomorrow){
					$tmp['mlist'][$k]['insert_time_m']    = date("H:i",$v['insert_time']);
				}else{
					$tmp['mlist'][$k]['insert_time_m']    = date("m-d",$v['insert_time']);
				}
				unset($user_name);
			}
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
}