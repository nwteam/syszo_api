<?php
//echo "sss";exit;
header("Content-Type: text/html; charset=UTF-8");
ini_set('date.timezone','Asia/Shanghai');
//定义CanPHP框架目录
define('CP_PATH',dirname(__FILE__).'/CanPHP/');//注意目录后面加“/”
define('__ROOTDIR__', str_replace('\\','/',realpath(dirname(__FILE__).'/'))."/");

require(dirname(__FILE__).'/config.php');//加载配置
require(CP_PATH.'core/cpApp.class.php');//加载应用控制类

$app=new cpApp($config);//实例化单一入口应用控制类
//执行项目
$app->run();

?>